#ifndef FUNCTION_H_
#define FUNCTION_H_

#include"LinkList.h"

int menu(); //荣单,每次功能使用结束后翻会调用讯视频 图片
void loadFromFile(LNode *L); //从文件加载数据
void saveToFile(LNode *L);   //保存数据到文件中
void s_gets(char *srcText);  //处理输入的字符串
void printOne(LNode *p) ;//用于单条评论的打印
void add(LNode *L);      //添加评论功能
void view(LNode *L);     //浏览所有评论
void del(LNode *L) ;     //删除评论
void modify(LNode *L);   //修改评论
#endif

