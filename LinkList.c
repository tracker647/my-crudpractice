#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#include"LinkList.h"

void InitList(LNode *L)
{
    LNode *p;
    p = L;
    p->prev = NULL;
    p->next = NULL;
    strcpy(p->comment.text, "Head");
}

LNode *InsList_FromTail(LNode *L)
{
    LNode *head,*newNode;
    LNode *p;
    head = L;
    int len = 0;
    newNode = (LNode *)malloc(sizeof(LNode));
    
    p = head;
    do  
    {
        p->id = len++;
        if(p->next != NULL)     //测量表长以标序,并让p指向最前端
            p = p->next;
        else 
            break;
    } while (1);

    p->next = newNode;
    newNode->next = NULL;
    newNode->prev = p;
    newNode->id = len;
    
    return head->next;
}

LNode *InsList_FromHead(LNode *L) //头插法
{
    LNode *head, *newNode;
    head = L;
    int len = 0;
    newNode = (LNode *)malloc(sizeof(LNode));
    if (head->next != NULL) //多评论情况
    {
        newNode->next = head->next;
        (head->next)->prev = newNode;
        head->next = newNode;
        newNode->prev = head;
    }
    else //只有一个评论情况
    {
        newNode->prev = head;
        head->next = newNode;
        newNode->next = NULL;
    }

    //测量表长以标序
    LNode *p;

    for (p = head; p != NULL; p = p->next)
    {
        p->id = len++;
    }

    return head->next;
}

int ListLength(LNode head)
{
    LNode *p;
    int len = 0;
    for (p = head.next; p != NULL; p = p->next)
    {
        len++;
    }
    return len;
}
void DelList(int index, LNode *L)
{
    int i = 0;
    LNode *p;
    for (p = L; i < index; i++, p = p->next)
    {
    }

    if (p->next != NULL) //要删除的在链表当中
    {
        (p->prev)->next = p->next;
        (p->next)->prev = p->prev;
        free(p);
    }
    else //要删除的在链表最前
    {
        (p->prev)->next = NULL;
        free(p);
    }
}
