#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "function.h"

char *filePath = "E:\\myprogramming\\C\\Practicing\\commentCRUD\\SingleFile\\data.txt"; //我的数据文件路径,你要换成自己的
int menu() //菜单，每次功能使用结束后都会调用
{
    int choose;
    printf(" ___________________________________\n");
    printf("|        欢迎使用学生留言系统v0.1  |\n");
    printf("|        您要:                     |\n");
    printf("|                1.留言            |\n");
    printf("|                2.查看留言        |\n");
    printf("|                3.删除留言        |\n");
    printf("|                4.修改留言        |\n");
    printf("|                5.退出            |\n");
    printf("|__________________________________|\n");
    printf("请按序号选择功能:\n");
    scanf("%d", &choose);
    return choose;
}
void loadFromFile(LNode *L) //从文件加载数据
{
    LNode *p, *newNode;
    FILE *fp;
    int order = 1;
    char line[MAX_SIZE];

    p = L;
    if ((fp = fopen(filePath, "rb")) == NULL)
    {
        fprintf(stdout, "Can't Open data file,Read failed\n");
        exit(EXIT_FAILURE);
    }
    else
    {
        fprintf(stdout, "数据文件读取成功!\n");
    }

    while (1)
    {
        newNode = (LNode *)malloc(sizeof(LNode));
        fread(&newNode->comment, sizeof(comment), 1, fp);
        if (feof(fp))
            break;
        p->next = newNode;
        newNode->prev = p;
        newNode->id = order++;
        newNode->next = NULL;
        p = newNode;
    }

    fclose(fp);
}
void saveToFile(LNode *L) //保存数据到文件中
{
    FILE *fp;
    LNode *p;

    if ((fp = fopen(filePath, "wb")) == NULL)
    {
        fprintf(stdout, "Can't Open data file,Read failed\n");
        exit(EXIT_FAILURE);
    }
    else
    {
        fprintf(stdout, "数据文件读取成功!\n");
    }

    if (L->next != NULL)
    {
        for (p = L->next; p != NULL; p = p->next)
        {
            fwrite(&p->comment, sizeof(comment), 1, fp); //依次把各结点数据"倒入"文件
        }
    }
    else //如果链表都被删光了
    {
        fclose(fp);
        return 0;
    }

    fclose(fp);
}
void s_gets(char *srcText) //输入的字符串要保留空格和换行符
{
    int i;
    for (i = 0; i < MAX_SIZE; i++)
    {
        srcText[i] = NULL;
    };
    fflush(stdin);
    gets(srcText);
    for (i = 0; srcText[i] != '\000'; i++)
    {
    }
    srcText[i] = '\n';
}
void add(LNode *L) //添加评论功能
{
    LNode *p;
    char srcText[MAX_SIZE];
    fflush(stdin);
    printf("请留言:\n");
    s_gets(srcText);

    if (srcText[0] == '\n')
    {
        printf("你未输入任何评论!\n");
        system("pause");
        return 0;
    }

    InsList_FromHead(L);
    p = L->next;
    strcpy(p->comment.text, srcText);
    printf("你的留言:\n%s\n", p->comment.text);

    //写入文件中
    saveToFile(L);
}
void printOne(LNode *p) //用于单条评论的打印
{
    printf("_______________________________\n");
    printf("#%d %s\n", p->id, p->comment.text);
    printf("_______________________________\n");
}
void view(LNode *L) //浏览所有评论
{
    system("cls");
    LNode *p;
    if (L->next == NULL)
    {
        printf("当前没有任何留言!\n");
        return 0;
    }
    printf("当前所有留言:\n");
    for (p = L->next; p != NULL; p = p->next)
    {
        printOne(p);
    }
    system("pause");
}
void del(LNode *L) //删除评论
{

    int i = 0;
    int len = 1;
    int index;
    char choose;
    LNode *p;

    if (ListLength(*L) == 0)
    {
        printf("当前没有任何可删除的评论!\n");
        return 0;
    }

    view(L);
    printf("输入要删除的评论序号:\n");
    scanf("%d", &index);
    for (p = L; i < index; i++, p = p->next)
    {
    }
    printf("评论内容:\n");
    printOne(p);
    fflush(stdin); //刷新输入流，避免下一个输入读取上一个输入的换行符
    printf("确定要删除吗? 确定选Y\n");
    scanf("%c", &choose);
    if (toupper(choose) == 'Y')
    {
        DelList(index, L);
        printf("评论已删除。\n");
        saveToFile(L);
    }
    system("pause");

    //删除评论，链表结构改变，要重新标序
    p = L;
    do
    {
        p = p->next;
        if (p == NULL)
        {
            return 0;
        }
        else
        {
            p->id = len++;
        }

    } while (1);

   
}
void modify(LNode *L) //修改评论
{
    LNode *p;
    int index;
    int i = 0;
    char choose;
    char srcText[MAX_SIZE];

    if (ListLength(*L) == 0)
    {
        printf("当前没有任何可修改的评论!\n");
        return 0;
    }
    view(L);
    printf("输入要修改的评论序号:\n");
    scanf("%d", &index);
    for (p = L; i < index; i++, p = p->next)
    {
    }
    printf("评论内容:\n");
    printOne(p);
    fflush(stdin); //刷新输入流，避免下一个输入读取上一个输入的换行符

    printf("确定要修改吗? 确定选Y\n");
    scanf("%c", &choose);
    if (toupper(choose) == 'Y')
    {
        printf("请留言:\n");
        fflush(stdin);

        s_gets(srcText);
        strcpy(p->comment.text, srcText);

        printf("你的留言:\n%s\n", p->comment.text);
        printf("评论已修改。\n");
    }
    system("pause");
    saveToFile(L);
}
