#ifndef LINKLIST_H_
#define LINKLIST_H_

#define MAX_SIZE 100
typedef struct comment //学生的数据，目前只有评论,可再扩展出学号，用户名，日期等信息
{
    char text[MAX_SIZE];
}comment;
typedef struct LNode //学生的数据用链表存储
{
    struct comment comment;
    int id; //节点序号
    struct LNode *prev;
    struct LNode *next;
} LNode;
LNode *head;
LNode *tail;

void InitList(LNode *L);                //初始化链表
LNode *InsList_FromTail(LNode *L);      //尾插法添加链表
LNode *InsList_FromHead(LNode *L);      //头插法添加链表
int ListLength(LNode head);             //测量表长度
void DelList(int index, LNode *L);      //按序号删除链表



#endif